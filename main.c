#include <math.h>
#include "graphics.h"
#include "title_scene.h"
#include "game_scene.h"

Texture *tex_logo, *tex_tile;

int main(int argc, char *argv[]){

	struct{
		void (*processEvents)(GLFWwindow *window, int *scene);
		void (*render)(double time, int *scene);
	} scenes [] = {
		{titlescene_processEvents, titlescene_render},
		{gamescene_processEvents, gamescene_render}
	};
	int scene = 0;

  double time, totaltime;
  unsigned frames = 0;
  GLFWwindow* window = openWindow(WIDTH, HEIGHT, "TRETRIS", false, false);

  loadAtlas("assets.atlas");
  tex_logo = getTexture("logo.png");
  tex_tile = getTexture("tile.png");

	//GLuint uniform_time = glGetUniformLocation(program, "time");

  GLuint framebuffer = createFramebuffer();

  while(!glfwWindowShouldClose(window)){
    time = glfwGetTime();
	
		updateEvents(window);	

		scenes[scene].processEvents(window, &scene);

		//glUniform1f(uniform_time, time);

		beginFramebuffer(framebuffer);

    scenes[scene].render(time, &scene);

		render();

		endFramebuffer(framebuffer);
		glfwSwapBuffers(window);
    totaltime += glfwGetTime() - time;
    frames++;
  }

  deleteTextures();
	glfwTerminate();
  printf("ms: %.3f | FPS: %.1f\n", totaltime / frames, (float)frames / time);
	return 0;
}
