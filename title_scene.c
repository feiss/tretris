#include "title_scene.h"
#include <math.h>

#define MIN(x,y) ((x) < (y) ? (x) : (y))

void titlescene_processEvents(GLFWwindow *window, int *scene){
	if (leftJustPressed() || keyJustPressed(GLFW_KEY_SPACE)){
		*scene = 1;
	}
}

void titlescene_render(double time, int *scene){
	extern Texture *tex_logo;
	glClear(GL_COLOR_BUFFER_BIT);
	drawQuad(
		tex_logo,
		WIDTH2 - floor(tex_logo->w),
		HEIGHT2 - floor(tex_logo->h),
		tex_logo->w, tex_logo->h,
		255, 255, 255, MIN(255, floor(time / 2 * 255)),
		0.f, 0.f);
}
