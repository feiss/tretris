#include "game_scene.h"
#include <math.h>

#define STAGEX 11
#define STAGEY 11
#define ROWS 18
#define COLS 10
#define TILESIZE 16

#define STAGE(x,y) (stage[(y) * COLS + (x)])

extern Texture *tex_tile;

static int level;
static double t;
static double step_duration = 1.0;

static int next_block;

static int stage[ROWS * COLS];

static int BLOCK_COLORS[][3] = {
	{255, 0, 0},
	{0, 255, 0},
	{255, 0, 255},
	{255, 255, 0},
	{0, 0, 255},
	{0, 255, 255},
	{255, 155, 10}
};

static int BLOCKS[][16] = {
	{
		1,1,1,1,
		0,0,0,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		0,1,1,0,
		1,1,0,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		1,1,0,0,
		0,1,1,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		1,1,0,0,
		1,1,0,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		1,1,1,0,
		1,0,0,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		0,0,1,0,
		1,1,1,0,
		0,0,0,0,
		0,0,0,0
	},
	{
		0,1,0,0,
		1,1,1,0,
		0,0,0,0,
		0,0,0,0
	}
};




static struct {
	int x, y;
	int w, h;
	int type;
	bool falling;
	int shape[16];
} block;

void new_block(){
	block.type = next_block == -1 ? rand () % 7 : next_block;
	next_block = rand() % 7;
	switch(block.type){
		case 0: block.w = 4; block.h = 1; break;
		case 1: block.w = 3; block.h = 2; break;
		case 2: block.w = 3; block.h = 2; break;
		case 3: block.w = 2; block.h = 2; break;
		case 4: block.w = 3; block.h = 2; break;
		case 5: block.w = 3; block.h = 2; break;
		case 6: block.w = 3; block.h = 2; break;
	}
	block.x = floor(COLS / 2 - block.w / 2);
	block.y = -1;
	memcpy(block.shape, BLOCKS[block.type], 16 * sizeof(int));
	block.falling = false;
}

void print_stage(){
	int x, y;
	printf("\n");
	for (y = 0; y < ROWS; y++){
		for (x = 0; x < COLS; x++){
			printf("%i ", STAGE(x,y));
		}
		printf("\n");
	}
}

void drop_lines(int from_line){
	int x, y;
	for (y = from_line; y >= 0; y--){
		for (x = 0; x < COLS; x++){
			STAGE(x, y) = y > 0 ? STAGE(x, y - 1) : 0;
		}
	}
}

void do_lines(){
	int x, y;
	bool full;
	for (y = ROWS - 1; y >= 0; y--){
		full = true;
		for (x = 0; x < COLS; x++){
			if (STAGE(x, y) == 0) { full = false; break; }
		}
		if (full) {
			drop_lines(y);
			y++;
		}
	}
}

void freeze_block(){
	int x, y;
	for (x = 0; x < 4; x++){
		for (y = 0; y < 4; y++){
			if (block.shape[y * 4 + x] == 0) continue;
			STAGE(block.x + x, block.y + y) = 1;
		}
	}
	do_lines();
}

bool check_colision(){
	int x, y;
	for (x = 0; x < 4; x++){
		for (y = 0; y < 4; y++){
			if (block.shape[y * 4 + x] == 0) continue;
			if (STAGE(block.x + x, block.y + y)) return true;
		}
	}
	return false;
}

void start_game(double time){
	level = 0;
	next_block = -1;
	new_block();
	t = time;
	memset(stage, 0, COLS * ROWS * sizeof(int));
}

void rotate(){
	int d = block.w > block.h ? block.w : block.h;
	int aux[16]; 
	int x, y;
	memcpy(aux, block.shape, 16 * sizeof(int));

	int offset = 0;
	for (y = 0; y < d; y++){
		bool emptyline = true;
		for (x = 0; x < d; x++){
			if (block.shape[y*4+x] != 0) { emptyline = false; break; }
		}
		if (emptyline) offset ++; else break;
	}

	for (y = 0; y < d; y++){
		for (x = 0; x < d; x++){
			block.shape[ (d - x - 1) * 4 + y ] = aux[ (y + offset) * 4 + x ];
		}		
	}
	int a = block.w;
	block.w = block.h;
	block.h = a;
	if (block.x + block.w >= COLS ) block.x = COLS - block.w;
}

void game_over(){
	t = 0;
}

void draw_tile(int x, int y, int r, int g, int b){
	drawQuad(tex_tile, x, y, tex_tile->w, tex_tile->h, r, g, b, 255, 0.f, 0.f);
}

void draw_block(int bx, int by, int type, int shape[]){
	int x, y;
	for (x = 0; x < 4; x++){
	for (y = 0; y < 4; y++){
		if (shape[y * 4 + x] == 0) continue;
		draw_tile(
			STAGEX + (bx + x) * TILESIZE * PIXEL_SIZE,
			STAGEY + (by + y) * TILESIZE * PIXEL_SIZE,
			BLOCK_COLORS[type][0], 
			BLOCK_COLORS[type][1], 
			BLOCK_COLORS[type][2]
		);
	}
}
}


void gamescene_processEvents(GLFWwindow *window, int *scene){
	if (keyJustPressed(GLFW_KEY_Q)){
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (rightJustPressed() || keyJustPressed(GLFW_KEY_UP)){
		rotate();
	}
	if (keyJustPressed(GLFW_KEY_F5)){
		t = 0;
		return;
	}
	if (keyJustPressed(GLFW_KEY_LEFT)){
		if (block.x > 0) block.x --;
		if (check_colision()) block.x ++;
	}
	if (keyJustPressed(GLFW_KEY_RIGHT)){
		if (block.x + block.w < COLS ) block.x ++;
		if (check_colision()) block.x --;
	}
	if (keyJustPressed(GLFW_KEY_SPACE) || keyJustPressed(GLFW_KEY_DOWN)){
		block.falling = true;
	}
	else if (keyJustReleased(GLFW_KEY_SPACE) || keyJustReleased(GLFW_KEY_DOWN)){
		block.falling = false;
	}
}

void gamescene_render(double time, int *scene){
	//init
	if (t == 0) { 
		start_game(time); 
	}
	else if ((!block.falling && time - t > step_duration) || (block.falling && time - t > step_duration / 50.0)){
		block.y ++;
		t = time;

		if (block.y + block.h == ROWS + 1 || check_colision()){
			block.y --;
			if (block.y < 0){
				game_over();
				*scene = 0;
				return;
			}
			freeze_block();
			new_block();
		}
	}

	int x, y, gray;

	glClear(GL_COLOR_BUFFER_BIT);
	// bg
	drawQuad(NULL, 0, 0, WIDTH, HEIGHT, 20, 20, 20, 255, 0.f, 0.f);
	// stage
	drawQuad(NULL, STAGEX, STAGEY, TILESIZE * COLS, TILESIZE * ROWS, 10, 10, 10, 255, 0.f, 0.f);
	for (x = 0; x < COLS; x++){
		for (y = 0; y < ROWS; y++){
			gray = STAGE(x,y) ? 120 : 15;
			draw_tile(
				STAGEX + x * TILESIZE * PIXEL_SIZE,
				STAGEY + y * TILESIZE * PIXEL_SIZE,
				gray, gray, gray
			);
		}
	}

	//next block
	draw_block(COLS + 1, 0, next_block, BLOCKS[next_block]);

	//active block
	if (block.y >= 0) {
		draw_block(block.x, block.y, block.type, block.shape);
	}
}
