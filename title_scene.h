#ifndef TITLE_SCENE
#define TITLE_SCENE
#include "graphics.h"

void titlescene_processEvents(GLFWwindow *window, int *scene);
void titlescene_render(double time, int *scene);


#endif